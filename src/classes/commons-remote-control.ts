import { Observable, Subject } from 'rxjs';

import { ICommonsNetworkPacket } from 'tscommons-network';

import { CommonsNetworkService } from '../services/commons-network.service';

import { ICommonsRemoteControlCommand, isICommonsRemoteControlCommand } from '../interfaces/icommons-remote-control-command';

export class CommonsRemoteControl {
	public onRemoteControlCommands: Map<string, Subject<any|undefined>> = new Map<string, Subject<any|undefined>>();
	
	constructor(
			private networkService: CommonsNetworkService,
			private ns: string
	) {
		this.networkService.receiveObservable().subscribe(
				(packet: ICommonsNetworkPacket): void => {
					if (packet.ns !== ns || packet.command !== 'remote-control') return;
					
					const remoteControlCommand: unknown = packet.data;
					if (!isICommonsRemoteControlCommand(remoteControlCommand)) {
						console.error('Invalid remote control packet received');
						return;
					}
					
					if (!this.onRemoteControlCommands.has(remoteControlCommand.command)) return;
					this.onRemoteControlCommands.get(remoteControlCommand.command)!.next(remoteControlCommand.data);
				}
		);
	}

	public remoteControlCommandObservable(command: string): Observable<any|undefined> {
		if (!this.onRemoteControlCommands.has(command)) this.onRemoteControlCommands.set(command, new Subject<any|undefined>());
		
		return this.onRemoteControlCommands.get(command)!;
	}

	public async sendCommand(command: string, data?: any): Promise<void> {
		const c: ICommonsRemoteControlCommand = {
				command: command,
				data: data
		};
		
		await this.networkService.broadcast(
				this.ns,
				'remote-control',
				c,
				CommonsNetworkService.expiryDate(60),
				false
		);
	}
}
