import { CommonsType } from 'tscommons-core';

export interface ICommonsRemoteControlCommand {
		command: string;
		data?: any;
}
export function isICommonsRemoteControlCommand(test: any): test is ICommonsRemoteControlCommand {
	if (!CommonsType.hasPropertyString(test, 'command')) return false;
	
	return true;
}
