import { ICommonsNetworkPacket } from 'tscommons-network';

import { CommonsNetworkService } from './commons-network.service';

export class CommonsNetworkMultiService extends CommonsNetworkService {
	protected peers: CommonsNetworkService[] = [];
	
	constructor() {
		super();
	}
	
	public addPeer(peer: CommonsNetworkService): void {
		this.peers.push(peer);
		
		peer.receiveObservable()
				.subscribe(
					(packet: ICommonsNetworkPacket): void => {
						this.incoming(packet);
					}
				);
	}

	public async transmit(packet: ICommonsNetworkPacket): Promise<any> {
		const promises: Promise<any>[] = [];
		for (const peer of this.peers) {
			promises.push(peer.transmit(packet));
		}
		
		await Promise.all(promises);
	}
	
	public setChannel(channel: string): void {
		super.setChannel(channel);
		for (const peer of this.peers) peer.setChannel(channel);
	}
	
	public setDeviceId(id?: string): void {
		super.setDeviceId(id);
		for (const peer of this.peers) peer.setDeviceId(id);
	}
}
