import { Observable, Subject } from 'rxjs';

import { CommonsBase62 } from 'tscommons-core';
import {
		ICommonsNetworkPacket,
		ETransmissionMethod
} from 'tscommons-network';

export abstract class CommonsNetworkService {
	public static generateDeviceId(): string {
		return CommonsBase62.generateRandomId();
	}
	
	public static generatePacketId(): string {
		return CommonsBase62.generateRandomId();
	}
	
	public static expiryDate(seconds: number): Date {
		const date: Date = new Date();
		date.setTime(date.getTime() + (seconds * 1000));
		
		return date;
	}
	
	private onReceive = new Subject<ICommonsNetworkPacket>();

	private channel?: string;
	private deviceId?: string;
	private lastSeen: string|undefined;
	private seen: string[] = [];
	
	public receiveObservable(): Observable<ICommonsNetworkPacket> {
		return this.onReceive;
	}

	protected incoming(packet: ICommonsNetworkPacket): void {
		if (packet.channel !== this.channel) return;
		if (packet.expiry && packet.expiry.getTime() < new Date().getTime()) return;

		if (this.seen.includes(packet.id)) return;
		this.seen.push(packet.id);
		
		if (!packet.replay) this.lastSeen = packet.id;
	
		if (
				(packet.method === ETransmissionMethod.BROADCAST && (!packet.ignoreOwn || (packet.source !== this.deviceId)))
				|| ((packet.method === ETransmissionMethod.DIRECT || packet.method === ETransmissionMethod.MULTICAST) && packet.destination === this.deviceId)
		) {
			this.onReceive.next(packet);
		}
	}
	
	public abstract async transmit(packet: ICommonsNetworkPacket): Promise<any>;

	public setChannel(channel: string): void {
		this.channel = channel;
	}

	public getChannel(): string|undefined {
		return this.channel;
	}
	
	public setDeviceId(id?: string): void {
		this.deviceId = id;
	}

	public getDeviceId(): string|undefined {
		return this.deviceId;
	}

	public getLastSeen(): string|undefined {
		return this.lastSeen;
	}
	
	public setLastSeen(uid: string): void {
		this.lastSeen = uid;
	}
	
	public isSelf(id: string): boolean {
		if (this.deviceId === undefined) return false;
		return this.deviceId === id;
	}
	
	public async broadcast(ns: string, command: string, data?: any, expiry?: Date, ignoreOwn?: boolean): Promise<void> {
		if (this.deviceId === undefined) throw new Error('No deviceId set prior to broadcasting');
		if (this.channel === undefined) throw new Error('No channel set prior to broadcasting');
		
		const packet: ICommonsNetworkPacket = {
			id: CommonsNetworkService.generatePacketId(),
			method: ETransmissionMethod.BROADCAST,
			channel: this.channel,
			source: this.deviceId,
			timestamp: new Date(),
			ns: ns,
			command: command
		};
		if (data) packet.data = data;
		if (ignoreOwn) packet.ignoreOwn = true;
		if (expiry) packet.expiry = expiry;
		
		await this.transmit(packet);
	}
	
	public async direct(destination: string, ns: string, command: string, data?: any, expiry?: Date): Promise<void> {
		if (this.deviceId === undefined) throw new Error('No deviceId set prior to direct');
		if (this.channel === undefined) throw new Error('No channel set prior to direct');
		
		const packet: ICommonsNetworkPacket = {
			id: CommonsNetworkService.generatePacketId(),
			method: ETransmissionMethod.DIRECT,
			channel: this.channel,
			destination: destination,
			source: this.deviceId,
			timestamp: new Date(),
			ns: ns,
			command: command
		};
		if (data) packet.data = data;
		if (expiry) packet.expiry = expiry;
		
		await this.transmit(packet);
	}
	
	public async multicast(destinations: string[], ns: string, command: string, data?: any, expiry?: Date): Promise<void> {
		if (this.deviceId === undefined) throw new Error('No deviceId set prior to multicast');
		if (this.channel === undefined) throw new Error('No channel set prior to multicast');
		
		const packet: ICommonsNetworkPacket = {
			id: CommonsNetworkService.generatePacketId(),
			method: ETransmissionMethod.MULTICAST,
			channel: this.channel,
			destinations: destinations,
			source: this.deviceId,
			timestamp: new Date(),
			ns: ns,
			command: command
		};
		if (data) packet.data = data;
		if (expiry) packet.expiry = expiry;
		
		await this.transmit(packet);
	}
}
