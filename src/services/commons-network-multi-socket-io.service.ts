import { Observable, Subject } from 'rxjs';

import { ICommonsNetworkHandshake } from 'tscommons-network';

import { ECommonsSignalStrength } from '../enums/ecommons-signal-strength';

import { CommonsNetworkMultiService } from './commons-network-multi.service';
import { CommonsNetworkSocketIoService } from './commons-network-socket-io.service';

export class CommonsNetworkMultiSocketIoService extends CommonsNetworkMultiService {
	private onAnyConnected: Subject<void>;
	private onNoneConnected: Subject<void>;
	private onPeerNumberChanged: Subject<number>;
	private onSignalStrengthChanged: Subject<ECommonsSignalStrength>;
	private onPeerConnected: Subject<string>;
	private onPeerDisconnected: Subject<string>;
	
	private connected: string[] = [];
	
	constructor(
			private urls: string[],
			compress: boolean
	) {
		super();

		this.onAnyConnected = new Subject<void>();
		this.onNoneConnected = new Subject<void>();
		this.onPeerNumberChanged = new Subject<number>();
		this.onSignalStrengthChanged = new Subject<ECommonsSignalStrength>();
		this.onPeerConnected = new Subject<string>();
		this.onPeerDisconnected = new Subject<string>();
		
		for (const url of this.urls) {
			const peer: CommonsNetworkSocketIoService = new CommonsNetworkSocketIoService(url, compress);
			super.addPeer(peer);
			
			peer.connectObservable().subscribe((): void => {
				this.connected.push(url);

				if (this.connected.length === 1) this.onAnyConnected.next();
				this.onPeerNumberChanged.next(this.connected.length);
				this.onPeerConnected.next(url);
				
				switch (this.connected.length) {
					case 0:	// just to catch it in case
						this.onSignalStrengthChanged.next(ECommonsSignalStrength.DISCONNECTED);
						break;
					case 1:
						this.onSignalStrengthChanged.next(ECommonsSignalStrength.LOW);
						break;
					case 2:
						this.onSignalStrengthChanged.next(ECommonsSignalStrength.MEDIUM);
						break;
					default:
						this.onSignalStrengthChanged.next(ECommonsSignalStrength.HIGH);
						break;
				}
			});
			
			peer.disconnectObservable().subscribe((): void => {
				if (this.connected.length === 0) {
					console.log('CommonsNetworkMultiSocketIo connection count has tried to go negative. This should not be possible?');
					return;
				}
				
				this.connected = this.connected
						.filter((u: string): boolean => u !== url);
				
				if (this.connected.length === 0) this.onNoneConnected.next();
				this.onPeerNumberChanged.next(this.connected.length);
				this.onPeerDisconnected.next(url);

				switch (this.connected.length) {
					case 0:
						this.onSignalStrengthChanged.next(ECommonsSignalStrength.DISCONNECTED);
						break;
					case 1:
						this.onSignalStrengthChanged.next(ECommonsSignalStrength.LOW);
						break;
					case 2:
						this.onSignalStrengthChanged.next(ECommonsSignalStrength.MEDIUM);
						break;
					default:
						this.onSignalStrengthChanged.next(ECommonsSignalStrength.HIGH);
						break;
				}
			});
		}
	}
	
	public getPeersUrl(): string[] {
		return this.urls.slice();
	}
	
	public getConnectedPeersUrl(): string[] {
		return this.connected.slice();
	}

	public connectObservable(): Observable<void> {
		return this.onAnyConnected;
	}

	public disconnectObservable(): Observable<void> {
		return this.onNoneConnected;
	}

	public peerNumberChangedObservable(): Observable<number> {
		return this.onPeerNumberChanged;
	}

	public signalStrengthChangedObservable(): Observable<ECommonsSignalStrength> {
		return this.onSignalStrengthChanged;
	}

	public peerConnectedObservable(): Observable<string> {
		return this.onPeerConnected;
	}

	public peerDisconnectedObservable(): Observable<string> {
		return this.onPeerDisconnected;
	}

	public connect(params?: ICommonsNetworkHandshake): boolean {
		let anySuccess: boolean = false;
		
		for (const peer of this.peers) anySuccess = (peer as CommonsNetworkSocketIoService).connect(params) || anySuccess;
		
		return anySuccess;
	}
	
	// sometimes various ngInits happen after the initial connect
	public triggerPeerNumberChanged(): void {
		this.onPeerNumberChanged.next(this.connected.length);
			
		switch (this.connected.length) {
			case 0:
				this.onSignalStrengthChanged.next(ECommonsSignalStrength.DISCONNECTED);
				break;
			case 1:
				this.onSignalStrengthChanged.next(ECommonsSignalStrength.LOW);
				break;
			case 2:
				this.onSignalStrengthChanged.next(ECommonsSignalStrength.MEDIUM);
				break;
			default:
				this.onSignalStrengthChanged.next(ECommonsSignalStrength.HIGH);
				break;
		}
	}

	public async requestReplay(): Promise<void> {
		for (const peer of this.peers) {
			(peer as CommonsNetworkSocketIoService).requestReplay();
		}
	}
}
