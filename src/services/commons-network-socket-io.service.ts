import { Observable, Subject } from 'rxjs';

import { CommonsType } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';
import {
		ICommonsNetworkPacket,
		isICommonsNetworkPacket,
		ICommonsNetworkPacketCompressed,
		isICommonsNetworkPacketCompressed,
		compressPacket,
		decompressPacket
} from 'tscommons-network';
import { ICommonsNetworkHandshake } from 'tscommons-network';

import { CommonsSocketIoClientService } from 'nodecommons-socket-io';

import { CommonsNetworkService } from './commons-network.service';

class SocketIoClientService extends CommonsSocketIoClientService {
	private static buildOptions(url: string): SocketIOClient.ConnectOpts {
		const options: SocketIOClient.ConnectOpts = {
				transports: [ 'websocket', 'polling' ]
		};
		
		const regexp: RegExp = /^http(?:s?):\/\/[^\/]+\/(.+)$/i;
		const match: RegExpExecArray|null = regexp.exec(url);
		
		if (match !== null) {
			options.path = `/${match[1]}${match[1].endsWith('/') ? '' : '/'}socket.io`;
		}
		
		return options;
	}

	private static buildUrl(url: string): string {
		const regexp: RegExp = /^(http(?:s?):\/\/[^\/]+\/).+$/i;
		const match: RegExpExecArray|null = regexp.exec(url);
		
		if (match !== null) return match[1];
		
		return url;
	}

	private onConnect: Subject<void> = new Subject<void>();
	private onDisconnect: Subject<void> = new Subject<void>();

	constructor(
			url: string,
			private callback: (data: any) => void
	) {
		super(
				SocketIoClientService.buildUrl(url),
				true,
				SocketIoClientService.buildOptions(url)
		);
		
		this.addConnectCallback(
				(): void => {
					this.onConnect.next();
				}
		);
		
		this.addDisconnectCallback(
				(): void => {
					this.onDisconnect.next();
				}
		);
	}

	protected setupOns(): void {
		this.on('rx', (data: any): void => {
			this.callback(data);
		});
	}
	
	public connectObservable(): Observable<void> {
		return this.onConnect;
	}
	
	public disconnectObservable(): Observable<void> {
		return this.onDisconnect;
	}
	
	public async emit(command: string, data: any): Promise<any> {
		return await super.emit(command, data);
	}
}

export class CommonsNetworkSocketIoService extends CommonsNetworkService {
	socketIoClientService: SocketIoClientService;
	
	constructor(
			url: string,
			private compress: boolean = false
	) {
		super();
		
		this.socketIoClientService = new SocketIoClientService(
				url,
				(data: any): void => {
					if (!CommonsType.isEncodedObject(data)) {
						console.log('Invalid encoded packet');
						return;
					}
					
					const decoded: TPropertyObject = CommonsType.decodePropertyObject(data);

					let packet: ICommonsNetworkPacket;
					
					if (this.compress) {
						if (!isICommonsNetworkPacketCompressed(decoded)) {
							console.log('Invalid compressed packet');
							return;
						}
						
						packet = decompressPacket(decoded);
					} else {
						if (!isICommonsNetworkPacket(decoded)) {
							console.log('Invalid packet');
							return;
						}
						
						packet = decoded;
					}
					
					this.incoming(packet);
				}
		);
	}

	public connectObservable(): Observable<void> {
		return this.socketIoClientService.connectObservable();
	}

	public disconnectObservable(): Observable<void> {
		return this.socketIoClientService.disconnectObservable();
	}
	
	public async transmit(packet: ICommonsNetworkPacket): Promise<any> {
		let encoded: TEncodedObject;
		
		if (this.compress) {
			const compressed: ICommonsNetworkPacketCompressed = compressPacket(packet);
			encoded = CommonsType.encodePropertyObject(compressed);
		} else encoded = CommonsType.encodePropertyObject(packet);
		
		return await this.socketIoClientService.emit('tx', encoded);
	}
	
	public async requestReplay(): Promise<void> {
		const since: string|undefined = this.getLastSeen();
		if (since === undefined) return;
		
		try {
			const lastSeen: string|undefined = await this.socketIoClientService.emit('replay', {
				since: since,
				channel: this.getChannel()!,
				deviceId: this.getDeviceId()!
			});
			if (lastSeen !== undefined) this.setLastSeen(lastSeen);
		} catch (e) {
			console.log('Replay request failed');
		}
		return;
	}

	public connect(params?: ICommonsNetworkHandshake): boolean {
		if (this.getDeviceId() === undefined) throw new Error('No deviceId set prior to connecting');
		if (this.getChannel() === undefined) throw new Error('No channel set prior to connecting');

		if (params === undefined) {
			params = {
					channel: this.getChannel()!,
					deviceId: this.getDeviceId()!
			};
		} else {
			params.channel = this.getChannel()!;
			params.deviceId = this.getDeviceId()!;
		}
				
		return this.socketIoClientService.connect(params);
	}
}
